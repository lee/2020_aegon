#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import logging
from urllib.parse import quote_plus, urlencode
from datetime import date
from pymongo import MongoClient
import json
import importlib
import requests
import traceback


my_module = importlib.import_module(
    os.path.splitext((os.path.basename(__file__)))[0] + "_config"
)
debug = my_module.debug
uri_template = my_module.uri_template
payload = my_module.payload
headers = my_module.headers
scheme = my_module.scheme
database = my_module.database
mongoServer = my_module.mongoServer
mongoPort = my_module.mongoPort
PER_PAGE = my_module.PER_PAGE
MAX_ROWS = my_module.MAX_ROWS

connection = MongoClient(mongoServer, mongoPort)
db = connection[database]


def get_logger():
    # Set up logging
    file_path = os.path.basename(sys.argv[0])
    filename = file_path.replace(".py", "")
    # logfile = "/tmp/" + filename + "_pension.log"
    logfile = filename + "_pension.log"
    logging.basicConfig(
        filename=logfile,
        format="%(asctime)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level=logging.INFO,
    )
    logger = logging.getLogger(__name__)
    logger.info("#####################")
    logger.info("Starting %s", file_path)

    return logger


def get_timestamp():
    today = date.today().toordinal()
    lastWeek = today - 7
    sunday = lastWeek - (lastWeek % 7)
    friday = sunday + 5
    timestamp = date.fromordinal(friday)

    return timestamp


timestamp = get_timestamp()


def get_dbfunds(funds_data):
    dbfunds = {}
    for doc in funds_data.find({}):
        fundname = doc["fundname"]
        last_data = doc["prices"][-1]
        price_string = (last_data["sell_price"], last_data["buy_price"])
        dbfunds[fundname] = price_string

    return dbfunds


def get_webfunds(ae_label, sedol_search):
    webfunds = dict()

    total = 0
    page_number = 1

    while True:
        pl = payload.copy()
        pl["PageNo"] = page_number
        pl["PageSize"] = PER_PAGE
        pl["RowIDs"] = ",".join(
            [
                str(i)
                for i in range(
                    PER_PAGE * (page_number - 1) + 1, page_number * PER_PAGE + 1
                )
            ]
        )
        pl["ProjectName"] = ae_label
        pl["EnableSedolSearch"] = sedol_search
        url = uri_template.format(quote_plus(json.dumps(pl)))

        try:
            page = requests.get(url)
            data = json.loads(page.text)
        except:
            logger = get_logger()
            logger.error(traceback.format_exc())
            logging.error("Error downloading page at: " + url)
            sys.exit(1)
        else:
            data = json.loads(data)
            data["TotalRows"] = data.get("TotalRows") or MAX_ROWS

        for item in data.get("DataList", []):
            fund = item.get("FundInfo", {}).get("Name")
            bid = item.get("Price", {}).get("Bid", {}).get("Amount")
            offer = item.get("Price", {}).get("Offer", {}).get("Amount")
            webfunds[fund] = (
                bid,
                offer,
            )

            logging.info("Fund: {}, Bid: {}, Offer: {}".format(fund, bid, offer))

        if (data.get("PageNo", 0) * PER_PAGE) < data["TotalRows"]:
            page_number += 1
        else:
            break

    return webfunds


def update_database(webfunds, funddata):  # pass in the list
    for fundname, values in webfunds.items():
        bid, offer = values

        if debug == 0:
            funddata.update(
                {"fundname": fundname},
                {
                    "$push": {
                        "prices": {
                            "date": str(timestamp),
                            "bid": bid,
                            "offer": offer
                        }
                    }
                },
                upsert=True,
            )
        else:
            print(
                "DEBUG: in update_database, would mongo $push funds_data.update webfund '",
                fundname,
                "' with bid: %s " % bid,
                "and offer: %s " % offer,
                timestamp,
            )
    # Ends update_database function
    return


def main(scheme, ae_label, enable_sedol):
    logger = get_logger()
    funds_data = db[scheme]
    webfunds = get_webfunds(ae_label, enable_sedol)
    update_database(webfunds, funds_data)
    logger.info("Completed - quitting")
    sys.exit(1)