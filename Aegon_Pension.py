#!/usr/bin/env python

from Aegon_Scraper import main


ae_label = "aegonportal"
enable_sedol = True

scheme = "Aegon_Pension"


if __name__ == "__main__":
    main(scheme, ae_label, enable_sedol)
