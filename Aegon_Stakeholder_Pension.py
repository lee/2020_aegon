#!/usr/bin/env python

from Aegon_Scraper import main


ae_label = "aegonpensionssh"
enable_sedol = False
scheme = "Aegon_Stakeholder_Pension"


if __name__ == "__main__":
    main(scheme, ae_label, enable_sedol)
