#!/usr/bin/env python

from Aegon_Scraper import main


ae_label = "aegonpensionssp"
enable_sedol = False

scheme = "Aegon_Single_Price_Pension"


if __name__ == "__main__":
    main(scheme, ae_label, enable_sedol)
