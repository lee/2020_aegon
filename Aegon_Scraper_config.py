debug = 0

# This should be overwritten when Aegon_Scraper.py is called by, Eg Aegon_Pension.py
scheme = None
#scheme = "Aegon_Single_Price_Pension"
#scheme = "Aegon_Pension"  # This should be overwritten when Aegon_Scraper.py is called by, Eg Aegon_Pension.py
#scheme = "Aegon_Stakeholder_Pension"  # This should be overwritten when Aegon_Scraper.py is called by, Eg Aegon_Pension.py

uri_template = "https://digitalfundservice.feprecisionplus.com/FundDataService.svc/GetUnitList?jsonString={}"

headers = {
    "accept": "*/*",
    "accept-encoding": "gzip, deflate, br",
    "accept-language": "q=0.9,en-US;q=0.8,en;q=0.7,fr;q=0.6,es;q=0.5",
    "dnt": "1",
    "origin": "https://digital.feprecisionplus.com",
    "referer": "https://digital.feprecisionplus.com/",
    "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36",
}

database = "2020_Aegon"
# mongoServer = '178.62.5.32'
mongoServer = "192.168.2.142"
mongoPort = 27018

payload = {
    "FilteringOptions": {
        "undefined": 0,
        "SearchText": "",
        "Ter": {},
        "RangeId": None,
        "RangeName": "",
        "CategoryId": None,
        "PriipProductCode": None,
        "DefaultCategoryId": None,
        "ForSaleIn": None,
        "ShowMainUnits": False,
        "MPCategoryCode": None,
    },
    "ProjectName": None,  # "aegonpensionssp"
    "LanguageCode": "en-GB",
    "LanguageId": "1",
    "Theme": "aegonp",
    "SortingStyle": "1",
    "PageNo": 1,
    "PageSize": 0,
    "OrderBy": "UnitName:init",
    "IsAscOrder": True,
    "OverrideDocumentCountryCode": None,
    "ToolId": "1",
    "PrefetchPages": 40,
    "PrefetchPageStart": 1,
    "OverridenThemeName": "aegonp",
    "ForSaleIn": "",
    "ValidateFeResearchAccess": False,
    "HasFeResearchFullAccess": False,
    "EnableSedolSearch": None,  #
    "RowCount": 0,
    "RowIDs": [],
}


MAX_ROWS = 1000
PER_PAGE = 50
