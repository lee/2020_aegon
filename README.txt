Igor's commentary on handover:

you can use Aegon_Scraper and its config as a basement for others if you want. you will probably have to redefine that function responsible for scraping for some sites but at least you wouldnt duplicate everything else if you deicide to use it

Lee Notes:
A cronjob runs each of the grabber scripts:
Aegon_Pension.py
Aegon_Single_Price_Pension.py
Aegon_Stakeholder_Pension.py

Each of the three scripts should use a {basename}_config.py config file to set its scheme and any other technical parameters specific to that script.

About the python environment:
On the grabber server, all py3 grabber scripts are run out of their own directory but using a py3 virtualenv installed in:
~/py3_env/

(Similarly, all py2 grabber scripts are run out of their own directory but using a py2 virtualenv installed in:
~/venv/)

Each script is called by a cronjob. The cronjob calls the script via a bash wrapper that sets up the correct virtualenv for the script.

Eg of cronjob:
15 11 * * 0 /home/lee/py3_env_wrapper.bash python /home/lee/bs_price_grabbers/2020_Aegon/Aegon_Pension.py

Contents of /home/lee/py3_env_wrapper.bash:
source /home/lee/py3_env/bin/activate
"$@"

Note: grabber scripts likely to need proxied requests built in because FE are strong on stopping scraping. Grabber server's proxy details are:
socks5://127.0.0.1:9999
